import { Injectable } from '@angular/core';
//import {HttpClientModule} from '@angular/core/http';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http:HttpClient) { }
  private _url: string="http://localhost/laravel/api/employees";
  getEmployee(){
    return this.http.get(this._url);
  }
}
