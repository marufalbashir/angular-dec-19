import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule,routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { EmployeeComponent } from './employee/employee.component';
import {EmployeeService} from './employee.service';
import {HttpClientModule} from '@angular/common/http';
import { RestApiComponent } from './rest-api/rest-api.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
//import { AboutComponent } from './about/about.component';
//import { ServiceComponent } from './service/service.component';
@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    EmployeeComponent,
    RestApiComponent,
    routingComponents,
    PageNotFoundComponent,
    EmployeeDetailsComponent   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
