import { Component, OnInit } from '@angular/core';
import {EmployeeService} from './../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
  
})
export class EmployeeComponent implements OnInit {

  public employees=[];
  public students =[];
  constructor(private _employeeService: EmployeeService,private _studentService: EmployeeService) { }
  //constructor(private _studentService: EmployeeService) { }

  ngOnInit() {
    this.employees=this._employeeService.getEmployees();
    this.students=this._employeeService.getStudents();
  }

}
