import { Component, OnInit } from '@angular/core';
import {EmployeeService} from './../employee.service';
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html' ,
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {


  
  public name="Maruf Al Bashir";
  public successText='text-success';
  public hasError=false;
  public message='';
  public displayName= false;
  public displayName2= false;
  public color = 'red';
  public colors =["red","green","blue","Yellow"];
  public today=new Date();
  public multiClass={
    'text-success': !this.hasError,
    'text-danger' : this.hasError,
    'text-italic' : this.successText

  }
  public showMessage($event){
    console.log(event)
    return this.message='Hello I am Onclick button event';
  }
  public employees=[];
  public students =[];
  constructor(private _employeeService: EmployeeService,private _studentService: EmployeeService) { }
    ngOnInit() {
    this.employees=this._employeeService.getEmployees();
    this.students=this._employeeService.getStudents();
  }


}
