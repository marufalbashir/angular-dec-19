import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent } from './about/about.component';
import {ServiceComponent } from './service/service.component';
import {TestComponent } from './test/test.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
const routes: Routes = [
  {path: '', redirectTo:'/about', pathMatch: 'full'},
  //{path: '', component: TestComponent},
  {path: 'about', component: AboutComponent},
  {path: 'service', component: ServiceComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents=[TestComponent,AboutComponent,ServiceComponent,PageNotFoundComponent];