import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class  EmployeeService{

  constructor() { }
  getEmployees(){
    
    return[
      {"id":1,"name":"Maruf","age":23,"gender":"Male"},
      {"id":2,"name":"Kafi Billha","age":23,"gender":"Male"},
      {"id":3,"name":"Sukhi","age":13,"gender":"Female"},
    ];
  }
  getStudents(){
    return[
      {"id":1,"name":"Student1","age":23,"gender":"Male"},
      {"id":2,"name":"Student2","age":43,"gender":"Male"},
      {"id":2,"name":"Student3","age":13,"gender":"Female"},
    ]
  }
}
